<?php

class ShortPost extends Post {

	public function parseArticle(){
		$fp = fopen("posts/$this->filename",'r');
		if(!$fp) return;

		$this->title = preg_replace('/title: (\w+)/', '${1}', fgets($fp));

		$this->content = "";
		while (($buffer = fgets($fp)) !== false){
			if(strpos($buffer, "[SEPARATOR]") !== false)
			{
				$this->content .= "... <a href='$this->url'>[Lire la suite]</a>";
				break;
			}

			$this->content .= $buffer;

		}

		$this->content = $this->parseMD($this->content);
		fclose($fp);
	}
}