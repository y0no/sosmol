<section class="main">
{foreach $posts as $post}
	<h2><a href="{$post->url}">{$post->title}</a></h2>
	<h5 class="date">{$post->timestamp|date_format:"%e %B %Y"}	</h3>
	{$post->content}
{/foreach}
</section>

{if $nbPage gt 1}
<div class="pagination">
<ul>
	{for $page=1 to $nbPage}
		{if $page eq $currentPage}
		<li class="active"><span>{$page}</span></li>
		{else}
		<li><a href="?page={$page}">{$page}</a></li>
		{/if}
	{/for}
</ul>
</div>
{/if}