<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
    <channel>
      <title>{$config.blog_title}</title>
      <description>{$config.blog_description}</description>
      <lastBuildDate>{$smarty.now|date_format:"%a, %d %b %Y %H:%M:%S"}</lastBuildDate>
      <link>{$config.base_url}</link>
        {foreach $posts as $post} 
          <item>
            <title>{$post->title}</title>
            <description><![CDATA['{$post->content}']]></description>
            <pubDate>{$post->timestamp|date_format:"%a, %d %b %Y %H:%M:%S"}</pubDate>
            <link>{$post->url}></link>
          </item>
        {/foreach}
    </channel>
</rss>