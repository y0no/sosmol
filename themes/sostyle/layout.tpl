  <!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>{$config.blog_title}</title>
  <base href="{$config.base_url}" />
  <meta name="description" content="{$config.blog_description}" />
  <link rel="stylesheet" type="text/css" href="{$config.theme_url}/css/styles.css"/>
  <link rel="stylesheet" href="{$config.theme_url}/css/monokai.css"/>
  <link rel="stylesheet" href="{$config.theme_url}/css/bootstrap.min.css"/>
  <link rel="alternate" type="application/rss+xml" title="{$config.blog_title}" href="{$config.base_url}/rss" />
  <script src="{$config.theme_url}/js/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>

<header>
  <h1><a href="{$config.base_url}">{$config.blog_title}</a></h1>
  <p class="description">{$config.blog_description}</p>
</header>

<div class="menu">
  <ul class="nav nav-pills">
    <li><a href="{$config.base_url}">Home</a></li>
    <li><a href="http://git.y0no.fr">Git</a></li>
    <li><a href="https://twitter.com/y0no">Twitter</a></li>
  </ul>
</div>

{include file="$file"}

<footer>
 {$smarty.now|date_format:"%Y"} &copy; {$config.author_name}. <br/>Powered by <a href="http://git.y0no.fr/sosmol">SoSmol</a>.
</footer>

</body>
</html>

