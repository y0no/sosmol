<?php /* Smarty version Smarty-3.1.13, created on 2013-05-16 21:25:07
         compiled from "themes/sostyle/layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:167786375951954eb396d103-98357629%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '79e9bdb65dae32718faaf0138e5003e95a7a8346' => 
    array (
      0 => 'themes/sostyle/layout.tpl',
      1 => 1367528505,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '167786375951954eb396d103-98357629',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_51954eb39cc267_71794894',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51954eb39cc267_71794894')) {function content_51954eb39cc267_71794894($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/core/smarty/plugins/modifier.date_format.php';
?>  <!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title><?php echo $_smarty_tpl->tpl_vars['config']->value['blog_title'];?>
</title>
  <base href="<?php echo $_smarty_tpl->tpl_vars['config']->value['base_url'];?>
" />
  <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['config']->value['blog_description'];?>
" />
  <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/css/styles.css"/>
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/css/monokai.css"/>
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/css/bootstrap.min.css"/>
  <link rel="alternate" type="application/rss+xml" title="<?php echo $_smarty_tpl->tpl_vars['config']->value['blog_title'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['base_url'];?>
/rss" />
  <script src="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/js/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>

<header>
  <h1><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['base_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['config']->value['blog_title'];?>
</a></h1>
  <p class="description"><?php echo $_smarty_tpl->tpl_vars['config']->value['blog_description'];?>
</p>
</header>

<div class="menu">
  <ul class="nav nav-pills">
    <li><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['base_url'];?>
">Home</a></li>
    <li><a href="http://git.y0no.fr">Git</a></li>
    <li><a href="https://twitter.com/y0no">Twitter</a></li>
  </ul>
</div>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['file']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<footer>
 <?php echo smarty_modifier_date_format(time(),"%Y");?>
 &copy; <?php echo $_smarty_tpl->tpl_vars['config']->value['author_name'];?>
. <br/>Powered by <a href="http://git.y0no.fr/sosmol">SoSmol</a>.
</footer>

</body>
</html>

<?php }} ?>