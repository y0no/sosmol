<section class="links">
	<ul class="unstyled">
	{foreach $posts as $post}
		<li><a href="{$post->url}">{$post->title}</a></li>
	{/foreach}
	</ul>
</section>
