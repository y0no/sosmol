<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>{$config.blog_title}</title>
  <base href="{$config.base_url}" />
  <meta name="description" content="{$config.blog_description}" />
  <link href='http://fonts.googleapis.com/css?family=PT+Mono' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="{$config.theme_url}/css/monokai.css"/>
  <link rel="stylesheet" href="{$config.theme_url}/css/base.css" media="all" />
  <link rel="stylesheet" href="{$config.theme_url}/css/layout.css" media="all" />
  <link rel="stylesheet" href="{$config.theme_url}/css/skeleton.css" media="all" />
  <link rel="stylesheet" href="{$config.theme_url}/css/style.css" media="all" />
  <link rel="alternate" type="application/rss+xml" title="{$config.blog_title}" href="{$config.base_url}/rss" />
  <script src="{$config.theme_url}/js/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>

<div align="center">
  <h1><a href="{$config.base_url}">{$config.blog_title}</a></h1>
  <p id="description">{$config.blog_description}</p>
</div>

<div class="container">
  <div class="three columns sidebar">
    <h3 id="logo">Navigation</h3>  
      <ul>
        <li><a href="http://y0no.fr">Blog</a></li>
	<li><a href="/list">Articles</a></li>
	<li><a href="/rss">Flux RSS</a></li>
        <li><a href="http://git.y0no.fr">Git</a></li>
        <li><a href="http://twitter.com/y0no">Twitter</a></li>
      </ul>
	<div class="pub">
	<a href="http://www.nuitduhack.com" target="_blank"><img src="http://www.nuitduhack.com/sites/all/themes/nuitduhack-dev/images/120-600_ndh.png" alt="Nuit Du Hack"></a>
	</div>
    </div>

    <div class="twelve columns content">
      {include file="$file"}
    </div>
  </div>
</div>

<div id="footer">
 {$smarty.now|date_format:"%Y"} &copy; {$config.author_name}. <br/>Powered by <a href="http://git.y0no.fr/sosmol">SoSmol</a>.
</div>

</body>
</html>

