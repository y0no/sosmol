<?php /* Smarty version Smarty-3.1.13, created on 2013-05-19 15:18:41
         compiled from "themes/retro/home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:495927480519690f6ca6623-08201946%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0157f76f6302f3ba1f839cfa094859c5b6b7f215' => 
    array (
      0 => 'themes/retro/home.tpl',
      1 => 1368976675,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '495927480519690f6ca6623-08201946',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_519690f6dd73a0_08170968',
  'variables' => 
  array (
    'posts' => 0,
    'post' => 0,
    'nbPage' => 0,
    'page' => 0,
    'currentPage' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_519690f6dd73a0_08170968')) {function content_519690f6dd73a0_08170968($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/core/smarty/plugins/modifier.date_format.php';
?><section class="main">
<?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value){
$_smarty_tpl->tpl_vars['post']->_loop = true;
?>
	<h2><a href="<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value->title;?>
</a></h2>
	<h5 class="date"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->timestamp,"%e %B %Y");?>
	</h3>
	<?php echo $_smarty_tpl->tpl_vars['post']->value->content;?>

<?php } ?>
</section>

<?php if ($_smarty_tpl->tpl_vars['nbPage']->value>1){?>
<ul class="pagination">
	<?php $_smarty_tpl->tpl_vars['page'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['page']->step = 1;$_smarty_tpl->tpl_vars['page']->total = (int)ceil(($_smarty_tpl->tpl_vars['page']->step > 0 ? $_smarty_tpl->tpl_vars['nbPage']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['nbPage']->value)+1)/abs($_smarty_tpl->tpl_vars['page']->step));
if ($_smarty_tpl->tpl_vars['page']->total > 0){
for ($_smarty_tpl->tpl_vars['page']->value = 1, $_smarty_tpl->tpl_vars['page']->iteration = 1;$_smarty_tpl->tpl_vars['page']->iteration <= $_smarty_tpl->tpl_vars['page']->total;$_smarty_tpl->tpl_vars['page']->value += $_smarty_tpl->tpl_vars['page']->step, $_smarty_tpl->tpl_vars['page']->iteration++){
$_smarty_tpl->tpl_vars['page']->first = $_smarty_tpl->tpl_vars['page']->iteration == 1;$_smarty_tpl->tpl_vars['page']->last = $_smarty_tpl->tpl_vars['page']->iteration == $_smarty_tpl->tpl_vars['page']->total;?>
		<?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->tpl_vars['currentPage']->value){?>
		<li class="active"><span><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
</span></li>
		<?php }else{ ?>
		<li><a href="?page=<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
</a></li>
		<?php }?>
	<?php }} ?>
</ul>
<?php }?><?php }} ?>