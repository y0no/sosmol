<?php /* Smarty version Smarty-3.1.13, created on 2013-05-19 15:18:39
         compiled from "themes/retro/layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:480569979519690e53b4064-44868680%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cf568a180d2167f313da3ca67094caa3d1fe8e69' => 
    array (
      0 => 'themes/retro/layout.tpl',
      1 => 1368976675,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '480569979519690e53b4064-44868680',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_519690e5451255_01965187',
  'variables' => 
  array (
    'config' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_519690e5451255_01965187')) {function content_519690e5451255_01965187($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/core/smarty/plugins/modifier.date_format.php';
?><!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title><?php echo $_smarty_tpl->tpl_vars['config']->value['blog_title'];?>
</title>
  <base href="<?php echo $_smarty_tpl->tpl_vars['config']->value['base_url'];?>
" />
  <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['config']->value['blog_description'];?>
" />
  <link href='http://fonts.googleapis.com/css?family=PT+Mono' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/css/monokai.css"/>
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/css/base.css" media="all" />
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/css/layout.css" media="all" />
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/css/skeleton.css" media="all" />
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/css/style.css" media="all" />
  <link rel="alternate" type="application/rss+xml" title="<?php echo $_smarty_tpl->tpl_vars['config']->value['blog_title'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['config']->value['base_url'];?>
/rss" />
  <script src="<?php echo $_smarty_tpl->tpl_vars['config']->value['theme_url'];?>
/js/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>

<div align="center">
  <h1><a href="<?php echo $_smarty_tpl->tpl_vars['config']->value['base_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['config']->value['blog_title'];?>
</a></h1>
  <p id="description"><?php echo $_smarty_tpl->tpl_vars['config']->value['blog_description'];?>
</p>
</div>

<div class="container">
  <div class="three columns sidebar">
    <h3 id="logo">Navigation</h3>  
      <ul>
        <li><a href="http://y0no.fr">Blog</a></li>
	<li><a href="/list">Articles</a></li>
	<li><a href="/rss">Flux RSS</a></li>
        <li><a href="http://git.y0no.fr">Git</a></li>
        <li><a href="http://twitter.com/y0no">Twitter</a></li>
      </ul>
	<div class="pub">
	<a href="http://www.nuitduhack.com" target="_blank"><img src="http://www.nuitduhack.com/sites/all/themes/nuitduhack-dev/images/120-600_ndh.png" alt="Nuit Du Hack"></a>
	</div>
    </div>

    <div class="twelve columns content">
      <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['file']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>
  </div>
</div>

<div id="footer">
 <?php echo smarty_modifier_date_format(time(),"%Y");?>
 &copy; <?php echo $_smarty_tpl->tpl_vars['config']->value['author_name'];?>
. <br/>Powered by <a href="http://git.y0no.fr/sosmol">SoSmol</a>.
</div>

</body>
</html>

<?php }} ?>