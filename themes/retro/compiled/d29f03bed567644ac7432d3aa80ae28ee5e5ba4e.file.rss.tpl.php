<?php /* Smarty version Smarty-3.1.13, created on 2013-05-17 20:19:53
         compiled from "themes/retro/rss.tpl" */ ?>
<?php /*%%SmartyHeaderCode:796693552519690e9da85a6-58599953%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd29f03bed567644ac7432d3aa80ae28ee5e5ba4e' => 
    array (
      0 => 'themes/retro/rss.tpl',
      1 => 1361626214,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '796693552519690e9da85a6-58599953',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'posts' => 0,
    'post' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_519690e9e99864_73769138',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_519690e9e99864_73769138')) {function content_519690e9e99864_73769138($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/core/smarty/plugins/modifier.date_format.php';
?><?php echo '<?xml';?> version="1.0" encoding="UTF-8"<?php echo '?>';?>

<rss version="2.0">
    <channel>
      <title><?php echo $_smarty_tpl->tpl_vars['config']->value['blog_title'];?>
</title>
      <description><?php echo $_smarty_tpl->tpl_vars['config']->value['blog_description'];?>
</description>
      <lastBuildDate><?php echo smarty_modifier_date_format(time(),"%a, %d %b %Y %H:%M:%S");?>
</lastBuildDate>
      <link><?php echo $_smarty_tpl->tpl_vars['config']->value['base_url'];?>
</link>
        <?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['posts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value){
$_smarty_tpl->tpl_vars['post']->_loop = true;
?> 
          <item>
            <title><?php echo $_smarty_tpl->tpl_vars['post']->value->title;?>
</title>
            <description><![CDATA['<?php echo $_smarty_tpl->tpl_vars['post']->value->content;?>
']]></description>
            <pubDate><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->timestamp,"%a, %d %b %Y %H:%M:%S");?>
</pubDate>
            <link><?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
></link>
          </item>
        <?php } ?>
    </channel>
</rss><?php }} ?>