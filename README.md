# Presentation

SoSmol est un système de blog léger ne nescessitant pas l'installation d'une base de donnée.
Les articles sont lus à partir de fichier écrit en markdown.

# Installation

1. Cloner le dépot git sur votre serveur web
2. Autoriser l'utilisation de fichier htaccess dans Apache
3. Modifier le fichier config.ini
4. Créer le dossier de cache (avec droit d'écriture), et le dossier contenant les articles (lecture seulement)
5. Résoudre le nombreux bugs qu'affiche votre serveur. 

# ToDo

- Un vrai système de cache fonctionnel
- Créer les régles de rewrite pour nginx, lighttpd
- Remplacer smarty par un moteur de template plus léger

